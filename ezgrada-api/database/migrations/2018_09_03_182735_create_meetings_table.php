<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->increments('id');
			$table->dateTime('datum');
            $table->unsignedInteger('broj_sednice');
            $table->unsignedInteger('ko_je_sazvao_sednicu');
	        $table->foreign('ko_je_sazvao_sednicu')->references('id')->on('users');		
			$table->string('mesto');
			$table->dateTime('zavrsena_vreme');
            $table->unsignedInteger('firma');
            $table->unsignedInteger('izvestajID');
            $table->unsignedInteger('ssID');
            $table->mediumText('opis');
            $table->longText('zapisnik');
            $table->binary('aktivan');
            $table->timestamps();
            $table->dateTime('pocela_vreme');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
