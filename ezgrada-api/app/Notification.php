<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['tekst', 'vreme', 'naslov', 'ssID', 'firma', 'aktivan', 'userID'];
}
