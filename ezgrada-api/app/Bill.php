<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
	protected $fillable = ['tip', 'broj_racuna', 'datum_izdavanja', 'ukupan_iznos'];

}
