<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
	protected $fillable = ['datum', 'broj_sednice', 'ko_je_sazvao_sednicu', 'mesto', 'zavrsena_vreme', 'firma', 'izvestajID', 'ssID', 'opis', 'zapisnik', 'aktivan', 'pocela_vreme'];
}
