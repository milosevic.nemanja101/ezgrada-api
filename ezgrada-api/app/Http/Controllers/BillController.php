<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bill;
use App\Http\Resources\BillResource;


class BillController extends Controller
{


    public function __construct()
    {
      $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BillResource::collection()->paginate(50));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
        Mozda je ovo pametnije?
        $Bill = Bill::create($request->all());

        return response()->json($Bill, 201);
        */
        $bill = Bill::create([
        'user_id' => $request->user()->id,
        'title' => $request->title,
        'description' => $request->description,
      ]);

      //$meeting = Meeting::create($request->all()); 
      
      return response()->json($bill, 201);
      //return new MeetingResource($meeting);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Bill $bill)
    {
         return new BillResource($bill);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bill $bill)
    {
        // check if currently authenticated user is the owner of the book
      if ($request->user()->id !== $meeting->user_id) {
        return response()->json(['error' => 'You can only edit your own meetings.'], 403);
      }

      $bill->update($request->only(['title', 'description']));
      //ILI
       //$meeting->update($request->all());


      //return new MeetingResource($meeting);
      return response()->json($bill, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bill $bill)
    {
        $bill->delete();
        return response()->json(null, 204);
    }
}

