<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Meeting;
use App\Http\Resources\MeetingResource;

class MeetingController extends Controller
{


	public function __construct()
    {
      $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return MeetingResource::collection()->paginate(50));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	/*
		Mozda je ovo pametnije?
    	$notification = Notification::create($request->all());

        return response()->json($notification, 201);
        */
        $meeting = Meeting::create([
        'user_id' => $request->user()->id,
        'title' => $request->title,
        'description' => $request->description,
      ]);

      //$meeting = Meeting::create($request->all()); 
      
      return response()->json($meeting, 201);
      //return new MeetingResource($meeting);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Meeting $meeting)
    {
         return new MeetingResource($meeting);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Meeting $meeting)
    {
        // check if currently authenticated user is the owner of the book
      if ($request->user()->id !== $meeting->user_id) {
        return response()->json(['error' => 'You can only edit your own meetings.'], 403);
      }

      $meeting->update($request->only(['title', 'description']));
      //ILI
       //$meeting->update($request->all());


      //return new MeetingResource($meeting);
      return response()->json($meeting, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meeting $meeting)
    {
        $meeting->delete();
        return response()->json(null, 204);
    }
}
