<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Notification;
use App\Http\Resources\NotificationResource;


class NotificationController extends Controller
{


    public function __construct()
    {
      $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return NotificationResource::collection()->paginate(50));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
        Mozda je ovo pametnije?
        $notification = Notification::create($request->all());

        return response()->json($notification, 201);
        */
        $notification = Notification::create([
        'user_id' => $request->user()->id,
        'title' => $request->title,
        'description' => $request->description,
      ]);

      //$meeting = Meeting::create($request->all()); 
      
      return response()->json($notification, 201);
      //return new MeetingResource($meeting);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
         return new NotificationResource($notification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        // check if currently authenticated user is the owner of the book
      if ($request->user()->id !== $meeting->user_id) {
        return response()->json(['error' => 'You can only edit your own meetings.'], 403);
      }

      $notification->update($request->only(['title', 'description']));
      //ILI
       //$meeting->update($request->all());


      //return new MeetingResource($meeting);
      return response()->json($notification, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        $$notification->delete();
        return response()->json(null, 204);
    }
}

