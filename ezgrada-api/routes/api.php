<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*Route::get('notifications', 'NotificationController@index');
Route::get('notifications/{notification}', 'Notification@show');
Route::post('notifications', 'Notification@store');
Route::put('notifications/{notification}', 'Notification@update');
Route::delete('notifications/{notification}', 'Notification@delete');
*/
Route::apiResource('notifications', 'NotificationController');
Route::apiResource('meetings', 'MeetingController');
Route::apiResource('bills', 'BillController');


Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
